function find(arr) {
    var count = arr.length;
    var not_found = true;
    var result = null;
    var center = Math.floor(count / 2);
    var min_index = 0;
    var max_index = count;
    while (not_found && center < count) {
        if (arr[center] == arr[center - 1]) {
            not_found = false;
            result = arr[center];
        } else {
            if (arr[center] == center + 1) {
                min_index = center;
                var diff = Math.floor((max_index - center) / 2);
                diff = diff == 0
                    ? 1
                    : diff;
                center += diff;
            } else {
                max_index = center;
                diff = Math.floor((center - min_index) / 2);
                diff = diff == 0
                    ? 1
                    : diff;
                center -= diff;
            }
        }
    }

    return result;
}

function test() {
    var arr = JSON.parse(window.document.getElementById('arr').value);
    window.document.getElementById('result').value = find(arr);
}
